package main

import (
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
)

var (
	basePath      = getRunDirectory()
	contentsCache = make(map[string]template.HTML)
	templateCache = make(map[string]template.HTML)
	templateName  = "_template.html"
)

type PageContents struct {
	HTML template.HTML
}

func notImplemented() {
	panic("Not Implemented")
}

func getRunDirectory() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return "/srv/gerc/"
	}
	fmt.Println(dir)
	return dir
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8089", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	var html template.HTML

	if _, ok := contentsCache[path]; !ok {
		html = getHTML(path)
		//registerHTMLCache(path, html)
	}

	template, err := getTemplate(path)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
	}

	body := PageContents{
		HTML: html,
	}

	template.Execute(w, body)
}

func getHTML(path string) template.HTML {
	var html template.HTML
	notImplemented()
	return html
}

func getTemplate(path string) (*template.Template, error) {
	fullPath := filepath.Join(basePath, path)

	templatePath, err := getTemplatePath(fullPath)

	if err != nil {
		return &template.Template{}, err
	}

	templ, err := template.New(templatePath).ParseFiles(templatePath)

	if err != nil {
		return &template.Template{}, err
	}

	return templ, nil
}

func getTemplatePath(templatePath string) (string, error) {

	if templatePath == basePath {
		return "", errors.New("Template not found")
	}

	contents, err := ioutil.ReadDir(templatePath)

	if err != nil {
		return "", nil
	}

	for _, file := range contents {
		if file.Name() == "_template.html" {
			return filepath.Join(templatePath, file.Name()), nil
		}
	}

	return getTemplatePath(filepath.Dir(templatePath))
}

func registerHTMLCache(path string, html template.HTML) {
	notImplemented()
}

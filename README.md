## Gerc

### What is this?

This is a simple and hopefully lightweight web framework inspired by cat-v's werc.

Instead of defining content in a database or in html, content is defined in markdown files. Eventually more filetypes should be supported such as html and text

### How far is this project

I just started it, but this project is fairly simple and should be finished before too long if I continue working on it

### TODO

Walk the entire directory path on startup to cache at least the paths used.

Add basic user support